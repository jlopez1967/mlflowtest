# Seguimiento de Proyectos de Machine Learning con MLflow
Con MLflow organizaremos los proyectos de machine learning, incluyendo los nombres de los experimentos, el responsable y los datos utilizados.

MLflow se ejecutará en una máquina virtual, específicamente en una versión de Linux, lo que permite una flexibilidad total en la gestión de los modelos, ya que básicamente se trata del procesamiento de archivos y se tiene control total sobre el sistema operativo.

En MLflow realizaremos la gestión de los experimentos, cada uno asociado al menos a un modelo. Existen procedimientos que permiten marcar o cambiar de estado el modelo asociado a un experimento, llevándolo desde "candidato a release" a "modelo productivo". También colocaremos etiquetas (tags) que pueden ser útiles para marcar la fecha en que un modelo fue implantado en producción.

# Selección y Despliegue de Modelos
Para seleccionar el mejor modelo, entrenaremos varios, comparando sus métricas y seleccionando el más adecuado. Este modelo seleccionado puede llevarse a una categoría como "pre-release" para realizar pruebas adicionales o pruebas manuales antes de pasar a producción. Cuando un modelo esté listo para producción, se le asignará una etiqueta llamada "Production".

Estos modelos pueden ser desplegados en producción de diferentes maneras: subiéndolos a un repositorio, transfiriéndolos por FTP, haciendo un commit en Git para que se desplieguen automáticamente con DevOps o creando un contenedor Docker y desplegándolo.

# Automatización y Escalabilidad
Para automatizar procesos, utilizaremos GitHub y procesos de automatización para ejecutar pruebas unitarias y publicar el modelo en producción. Dependiendo del tamaño del proyecto, se puede orquestar con Kubernetes para automatizar el despliegue y distribución del nuevo modelo.

# Gestión de Requerimientos y Soporte al Cliente
Es crucial tener claro lo que se quiere y implementar la infraestructura deseada por etapas. Se debe comenzar por organizar los experimentos y registrarlos, lo cual se puede hacer con un entrenamiento básico al personal que entrena los modelos actualmente. Además, es recomendable contratar DataBricks, un servicio de aplicación instalada en un nodo en Azure, AWS o Google Cloud, para organizar el proyecto. Es importante también estructurar una base de datos para llevar las versiones de los datos para cada experimento y comenzar a colocar métricas y llevar un control estricto de los logs para saber qué está pasando con la API entregada al cliente. De seguro faltan muchas más tareas.

# Futuras Acciones
Una vez que se haya abordado lo urgente aquellas tareas que puedan estar agobiando a la empresa, y luego es importante trabajar en las tareas importantes para que se minicen las tareas urgentes.

A largo plazo, un objetivo es tener bien documentados todos los procedimientos en un sistema de documentación estilo MkDocs (similar a este documento). Esto permitirá que las personas puedan comenzar un proyecto fácilmente y adecuadamente siguiendo la metodología planteada. También se pueden tener plantillas de tipo de proyecto, como proyectos de clasificación o regresión, para facilitar el inicio de nuevos proyectos.

En resumen, utilizar MLflow en los proyectos de machine learning puede ser altamente beneficioso, siempre y cuando se organice y gestione de manera efectiva. La implementación de las recomendaciones mencionadas puede ayudar a optimizar tus proyectos y garantizar su éxito a largo plazo.

## Ejemplo simple[](#Ejemplo-simple)

In [ ]:

import mlflow

import mlflow.sklearn

from sklearn.ensemble import RandomForestRegressor

from sklearn.model_selection import train_test_split

from sklearn.datasets import fetch_california_housing

import pandas as pd

from sklearn.metrics import mean_absolute_error,mean_squared_error,r2_score

import os

\# Carga de datos

california = fetch_california_housing()

X = california.data

y = california.target

\# Crear un DataFrame para el dataset completo

data = pd.DataFrame(X, columns=california.feature_names)

data['target'] = y

\# División de datos en entrenamiento y prueba

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

\# Guardar los datos completos y las divisiones en archivos CSV

data.to_csv("/databricks/data/full_dataset.csv", index=False)

pd.DataFrame(X_train, columns=california.feature_names).to_csv("/databricks/data/X_train.csv", index=False)

pd.DataFrame(X_test, columns=california.feature_names).to_csv("/databricks/data/X_test.csv", index=False)

pd.DataFrame(y_train, columns=["target"]).to_csv("/databricks/data/y_train.csv", index=False)

pd.DataFrame(y_test, columns=["target"]).to_csv("/databricks/data/y_test.csv", index=False)

\# Inicio del run

with mlflow.start_run() as run:

\# Registrar los archivos de datos

mlflow.log_artifact("/databricks/data/full_dataset.csv")

mlflow.log_artifact("/databricks/data/X_train.csv")

mlflow.log_artifact("/databricks/data/X_test.csv")

mlflow.log_artifact("/databricks/data/y_train.csv")

mlflow.log_artifact("/databricks/data/y_test.csv")

\# Definir y entrenar el modelo

model = RandomForestRegressor()

model.fit(X_train, y_train)

\# Registrar parámetros y métricas

mlflow.log_param("n_estimators", model.n_estimators)

mlflow.log_param("random_state", model.random_state)

mlflow.log_metric("train_score", model.score(X_train, y_train))

mlflow.log_metric("test_score", model.score(X_test, y_test))

mlflow.log_metric("mean_absolute_error", mean_absolute_error(y_test, model.predict(X_test)))

mlflow.log_metric("mean_squared_error", mean_squared_error(y_test, model.predict(X_test)))

mlflow.log_metric("r2_score", r2_score(y_test, model.predict(X_test)))

\# Guardar el modelo

mlflow.sklearn.log_model(model, "model")

print("Experimento completado y registrado en MLflow.")

2024/05/19 21:39:08 WARNING mlflow.models.model: Model logged without a signature. Signatures will be required for upcoming model registry features as they validate model inputs and denote the expected schema of model outputs. Please visit https://www.mlflow.org/docs/2.9.2/models.html\#set-signature-on-logged-model for instructions on setting a model signature on your logged model.

Experimento completado y registrado en MLflow.

## Ejemplo multiples modelos y optimización de parámetros[](#Ejemplo-multiples-modelos-y-optimizació)

In [ ]:

import mlflow

import sklearn

import mlflow.sklearn

from sklearn.ensemble import RandomForestRegressor

from sklearn.linear_model import LinearRegression, Ridge, Lasso

from sklearn.svm import SVR \# Support Vector Regressor

from xgboost import XGBRegressor

from sklearn.model_selection import train_test_split, GridSearchCV

from sklearn.datasets import fetch_california_housing

import pandas as pd

from sklearn.metrics import mean_squared_error, mean_absolute_error, r2_score, roc_auc_score

from mlflow.models.signature import infer_signature

\# Carga de datos

california = fetch_california_housing()

X = california.data

y = california.target

\# Crear un DataFrame para el dataset completo

data = pd.DataFrame(X, columns=california.feature_names)

data['target'] = y

\# División de datos en entrenamiento y prueba

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

\# Lista de modelos y sus hiperparámetros para buscar en GridSearch

models = {

'RandomForestRegressor': (RandomForestRegressor(), {'n_estimators': [50, 100, 200], 'max_depth': [None, 10, 20]}),

'LinearRegression': (LinearRegression(), {}),

'Ridge': (Ridge(), {'alpha': [0.1, 1.0, 10.0]}),

'Lasso': (Lasso(), {'alpha': [0.1, 1.0, 10.0]}),

'XGBRegressor': (XGBRegressor(), {'n_estimators': [50, 100, 200], 'max_depth': [3, 5, 7]})

}

\# Inicio del run principal

with mlflow.start_run() as main_run:

mlflow.log_artifacts("/databricks/data", artifact_path="data")

\# Entrenamiento y evaluación de modelos

for model_name, (model, param_grid) in models.items():

with mlflow.start_run(nested=True, run_name=model_name):

try:

mlflow.log_param("model_name", model_name) \# Registrar el nombre del modelo en el nested run

\# Optimización de hiperparámetros con GridSearchCV

grid_search = GridSearchCV(model, param_grid, scoring='neg_mean_squared_error', cv=5)

grid_search.fit(X_train, y_train)

\# Obtener el mejor modelo

best_model = grid_search.best_estimator\_

\# Predicciones de clase y probabilidad

y_pred = best_model.predict(X_test)

y_pred_proba = best_model.predict_proba(X_test)[:,1] if hasattr(best_model, 'predict_proba') else None

signature = infer_signature(X_train, best_model.predict(X_train))

\# Métricas de prueba

mlflow.log_metric("test_score", r2_score(y_test, y_pred))

mlflow.log_metric("mse", mean_squared_error(y_test, y_pred))

mlflow.log_metric("rmse", mean_squared_error(y_test, y_pred, squared=False))

mlflow.log_metric("mae", mean_absolute_error(y_test, y_pred))

\# Registrar los mejores hiperparámetros

for param, value in grid_search.best_params_.items():

try:

mlflow.log_param(param, value)

except Exception as e:

print(f"Error al registrar el parámetro {param}: {e}")

continue

\# Guardar el mejor modelo

conda_env = mlflow.sklearn.get_default_conda_env()

mlflow.sklearn.log_model(best_model, "model", conda_env=conda_env,signature=signature)

except Exception as e:

print(f"Error durante el entrenamiento y evaluación del modelo {model_name}: {e}")

continue

print("Experimento completado y registrado en MLflow.")

2024/05/19 21:40:32 INFO mlflow.store.artifact.cloud_artifact_repo: The progress bar can be disabled by setting the environment variable MLFLOW_ENABLE_ARTIFACTS_PROGRESS_BAR to false

#### Registrar el modelo en MLFlow Model Registry

Busca el mejor modelo el que tenga test Score mayor, aca se puede agregar el criterio que se desee

In [ ]:

import time

\# Get the experiment ID for the experiment with the highest test score

experiment_id = mlflow.search_runs().sort_values(by='metrics.test_score', ascending=False).iloc[0].experiment_id

\# Get the run ID for the run with the highest test score in the selected experiment

run_id = mlflow.search_runs(experiment_ids=[experiment_id]).sort_values(by='metrics.test_score', ascending=False).iloc[0].run_id

\# If you see the error "PERMISSION_DENIED: User does not have any permission level assigned to the registered model",

\# the cause may be that a model already exists with the name "wine_quality". Try using a different name.

model_name = "california_prerealese"

model_version = mlflow.register_model(f"runs:/{run_id}/model", model_name)

\# Registering the model takes a few seconds, so add a small delay

time.sleep(15)

Registered model 'california_prerealese' already exists. Creating a new version of this model...

Created version '4' of model 'ambiente.default.california_prerealese'.

## Mover el modelo a Produccion

Se coloco un alias en produccion para que pueda ser facilmente indentificable

In [ ]:

from mlflow.tracking import MlflowClient

client = MlflowClient()

client.set_registered_model_alias(model_name, "PRODUCTION", model_version.version)

In [ ]:

from mlflow.tracking import MlflowClient

client = MlflowClient()

model_name = "california_production"

model_version = mlflow.register_model(f"runs:/{run_id}/model", model_name)

\# Registering the model takes a few seconds, so add a small delay

time.sleep(15)

## Experimentos[](#Experimentos)

Ejecutando los experimentos

![](media/c9aee716ea9a499a83d61adf4b291dbd.png)

Esto es un ejemplo de la visualización de los experimentos después de ejecutarse y registrarse

![](media/58df6856f353676c0c51dfb9fe230c50.png)

Comparando 5 de los modelos entrenados

![](media/c0e6a6e3459a27af45d845ad21f0948d.png)

Ejemplo de las métricas

![](media/dff491372c9dabf4916fe1f9f1b1804f.png)

![](media/234282d18f52189f594bc18c0ee06217.png)
